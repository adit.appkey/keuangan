<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_flow_data', function (Blueprint $table) {
            $table->id();
            $table->foreignId('date_id')->constrained('cash_flow_dates');
            $table->string('description');
            $table->decimal('price');
            $table->enum('type', ['pengeluaran', 'pemasukan']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_flow_data');
    }
};
