<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cashFlowDate extends Model
{
    use HasFactory;
    
    protected $guarded = ['id'];
    protected $fillable = [
        'name',
        'start_date',
        'end_date',
        'created_at',
        'updated_at',
    ];
}
