<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cashFlowData extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $fillable = [
        'description',
        'price',
        'type',
        'created_at',
        'updated_at',
    ];
}
